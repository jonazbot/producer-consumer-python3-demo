#! usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This program demonstrates the Producer-Consumer problem, also known as Bounded-Buffer,
using concurrency with Producer and Consumer each running as a separate Process from
the multiprocessing module.
"""

__author__ = 'Jonas Valen'

from sys import exit
from time import sleep
import random
from multiprocessing import Process, Semaphore, Queue, Manager

BUFFER = Manager().list()   # Critical section - "Fridge".
BUFFER_SIZE = 5             # Max size of BUFFER - 'Fridge slots'.
LOCK = Semaphore(1)         # Semaphore.
SPEED = 0.2                 # Additional time for thread sleep. 0 = off.
RANDOMIZATION = 2           # Randomization multiplier for thread sleep.
                            # 1 = multiplier off, 0 = randomization off.


class Producer(Process):
    """
    Producer for bounded-buffer
    
    Producer class (mother) takes items from the 'items' list and 
    attempts to add them to the BUFFER (fridge) list.  If BUFFER is full 
    Producer will wait for a signal from Consumer (Kids) to resume 
    producing. 
    """

    def __init__(self):
        Process.__init__(self)

    def run(self):
        """Runs Producer"""

        items = ['Apple', 'Banana', 'Orange', 'Pear', 'Carrot', 'Melon',
                 'Jam', 'Milk', 'Yogurt', 'Cheese', 'Salami', 'Juice']

        while True:
            LOCK.acquire()
            if len(BUFFER) == BUFFER_SIZE:
                queue.put('Fridge is FULL!\n'
                          'Mother is WAITING to add more food.\n'
                         f'Fridge: {BUFFER}')
                LOCK.release()
                while len(BUFFER) == BUFFER_SIZE:
                    sleep(1)
                continue
            elif len(BUFFER) < BUFFER_SIZE:
                for n in range(random.randrange(BUFFER_SIZE - len(BUFFER)) + 1):
                    produced_item = random.choice(items)
                    BUFFER.append(produced_item)
                    queue.put(f'Mother ADDED {produced_item}')

            LOCK.release()
            sleep(random.random() * RANDOMIZATION + SPEED)


class Consumer(Process):
    """
    Consumer for bounded-buffer demo
    
    Consumer class (Kids) takes items from the BUFFER' list and attempts 
    to remove them.  If 'BUFFER' is empty the Consumer will wait for a 
    signal from Producer (Mother) to resume consuming. 
    """

    def __init__(self):
        Process.__init__(self)

    def run(self):
        """Runs Consumer"""

        while True:
            LOCK.acquire()
            if len(BUFFER) == 0:
                queue.put(f'Fridge is EMPTY!\n'
                          'Kids are WAITING for mother to add food.')
                LOCK.release()
                while len(BUFFER) == 0:
                    sleep(1)
                continue
            elif len(BUFFER) > 0:
                for n in range(random.randrange(len(BUFFER)) + 1):
                    consumed_item = BUFFER.pop(random.randrange(0, len(BUFFER)))
                    queue.put(f'Kids have CONSUMED {consumed_item}')

            LOCK.release()
            sleep(random.random() * RANDOMIZATION + SPEED)


if __name__ == "__main__":
    """Initializes and runs Producer and Consumer threads."""

    print(f'-----Bounded Buffer Demo-----\n'
          + 'Producer:      Mother\n'
          + 'Consumer:      Kids\n'
          + 'Shared Buffer: Fridge\n'
          + f'Buffer Size:   {BUFFER_SIZE}\n'
          + '-----------------------------\n')

    queue = Queue()
    producer = Producer()
    producer.daemon = True
    consumer = Consumer()
    consumer.daemon = True

    producer.start()
    consumer.start()

    try:
        print('Starting...\n')
        while True:
            print(queue.get())
    except (KeyboardInterrupt, SystemExit):
        print('\nTerminating...')
        producer.terminate()
        consumer.terminate()
        producer.join()
        consumer.join()
    exit()
